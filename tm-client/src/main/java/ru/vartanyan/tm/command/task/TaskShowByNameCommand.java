package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.endpoint.Task;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-show-by-name";
    }

    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findTaskByName(name, session);
    }

}
