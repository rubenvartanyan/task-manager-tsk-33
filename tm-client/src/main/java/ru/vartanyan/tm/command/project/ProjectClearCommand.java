package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-clearTasks";
    }

    @Override
    public String description() {
        return "Clear all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        if (bootstrap == null) throw new NullObjectException();
        final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        endpointLocator.getProjectEndpoint().clear(session);
        System.out.println("[OK]");
    }

}
