package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-change-password";
    }

    @Override
    public String description() {
        return "Change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE PASSWORD]");
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[ENTER NEW PASSWORD]");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        endpointLocator.getAdminEndpoint().setUserPassword(session.getUserId(), newPassword, session);
    }

}
