package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractEntity{

    @NotNull private String login;

    @NotNull private String passwordHash;

    @Nullable private String email;

    @Nullable private String firstName;

    @Nullable private String lastName;

    @Nullable private String middleName;

    @NotNull private Role role = Role.USER;

    @NotNull private Boolean locked = false;

}
