package ru.vartanyan.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.endpoint.*;
import ru.vartanyan.tm.api.repository.*;
import ru.vartanyan.tm.api.service.*;

import ru.vartanyan.tm.component.Backup;
import ru.vartanyan.tm.endpoint.*;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.repository.*;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.SystemUtil;
import ru.vartanyan.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;


@Getter
public final class Bootstrap implements ServiceLocator{

    @NotNull
    public final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    public final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    public final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    public final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IBackupService backupService = new BackupService(this);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    public final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final ISessionService sessionService = new SessionService(this, sessionRepository);

    @NotNull
    public final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    public final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    public final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    public final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    public final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    public final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    public final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @Nullable
    private Session session = null;

    public Bootstrap() {
    }

    void initUser() throws Exception {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
    }

    private void initBackup() {
        backup.init();
    }

    public void init() throws Exception {
        initPID();
        initUser();
        initBackup();
        initEndpoint();
    }

}
