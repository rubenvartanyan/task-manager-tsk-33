package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.IUserEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public User findUserByLogin(@WebParam(name = "login", partName = "login") @NotNull final String login,
                                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    @Override
    public void removeUserByItsLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                                     @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().removeByLogin(login);
    }

}
